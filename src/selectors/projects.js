// @flow
import type { State } from '../types'
import type { ProjectsState, Projects, Project } from '../types/projects'

export function selectProjects(state: State): ProjectsState {
  return state.entities.projects
}

export function selectCurrentProject(state: State, id: number): Project | void {
  const items: Projects = state.entities.projects.items
  return items.find(item => item.id === id)
}
