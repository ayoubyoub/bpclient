// @flow

import React, { Component } from 'react'

import type { Project } from '../../types/projects'

type Props = {
  project?: Project,
  onSubmit: (payload: { projectName: string, projectIdentifier: string, description: string }) => void
}

type State = {
  projectName: string,
  projectIdentifier: string,
  description: string,
  error: string
}

export default class ProjectsForm extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    // this is a controlled form, so
    let projectName = ''
    let projectIdentifier = ''
    let description = ''

    this.props.project && ({ projectName, projectIdentifier, description } = this.props.project)

    this.state = {
      projectName,
      projectIdentifier,
      description,
      error: ''
    }
  }

  handleTitleChange = (e: SyntheticEvent<HTMLInputElement>) => {
    const projectName = e.currentTarget.value
    this.setState(() => ({ projectName }))
  }

  handleIdChange = (e: SyntheticEvent<HTMLInputElement>) => {
    const projectIdentifier = e.currentTarget.value
    this.setState(() => ({ projectIdentifier }))
  }

  handleBodyChange = (e: SyntheticEvent<HTMLInputElement>) => {
    const description = e.currentTarget.value
    this.setState(() => ({ description }))
  }

  

  // Just some simple validations
  // (for illustration purposes, atm)
  handleSubmit = (e: SyntheticEvent<*>) => {
    e.preventDefault()

    let { projectName, projectIdentifier, description } = this.state
    projectName = projectName.trim()
    projectIdentifier = projectIdentifier.trim()
    description = description.trim()

    if (!projectName || !description || !projectIdentifier) {
      this.setState(() => ({
        error: 'Please provide both projectName, description and projectIdentifier.'
      }))
      return
    }

    const limit = 3000
    if (description.length > limit) {
      this.setState(() => ({
        error: `Body is too long (max. ${limit} characters)`
      }))
      return
    }

    this.setState(() => ({ error: '' }))
    this.props.onSubmit({ projectName, projectIdentifier, description })
  }

  render() {
    const { projectName, projectIdentifier, description, error } = this.state

    return (
      <div>
        {error && <div className="form-error">{error}</div>}

        <form className="form" onSubmit={this.handleSubmit}>
          <div className="field field-text">
            <label htmlFor="projectName">
            ProjectName:
              <input
                type="text"
                id="projectName"
                value={projectName}
                onChange={this.handleTitleChange}
              />
            </label>
          </div>

          <div className="field field-text">
            <label htmlFor="projectIdentifier">
            projectIdentifier:
              <input
                type="text"
                id="projectIdentifier"
                value={projectIdentifier}
                onChange={this.handleIdChange}
              />
            </label>
          </div>

          <div className="field field-text">
            <label htmlFor="body">
              Description:
              <textarea
                id="description"
                value={description}
                onChange={this.handleBodyChange}
              />
            </label>
          </div>

          <button className="btn">Save</button>
        </form>
      </div>
    )
  }
}
