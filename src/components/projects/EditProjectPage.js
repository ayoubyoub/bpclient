// @flow

import React, { Component } from 'react'
import { connect } from 'react-redux'
import navigateTo from '../../services/navigation'
import { UPDATE_PROJECT } from '../../actionTypes'
import { selectCurrentProject } from '../../selectors/projects'
import ProjectsForm from './ProjectsForm'

import type { Connector } from 'react-redux'
import type { State, Dispatch } from '../../types'
import type { Project, ProjectPayload as Payload } from '../../types/projects'

type Props = {
  project: Project,
  updateProject(payload: Payload): void
}

type OwnProps = {
  match: {
    params: {
      id: number
    }
  }
}

class EditProjectPage extends Component<Props> {
  handleSubmit = (payload: Payload) => {
    const { id } = this.props.project
    payload = { ...payload, id }
    this.props.updateProject(payload)
    navigateTo('/admin/projects')
  }

  render() {
    const { project } = this.props

    return (
      <div>
        <h2>Edit project</h2>
        {project && <ProjectsForm project={project} onSubmit={this.handleSubmit} />}
      </div>
    )
  }
}

function mapStateToProps(state: State, ownProps: OwnProps) {
  const project = selectCurrentProject(state, Number(ownProps.match.params.id))
  return {
    project
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    updateProject: (payload: Payload) => dispatch({ type: UPDATE_PROJECT, payload })
  }
}

const connector: Connector<OwnProps, Props> = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default connector(EditProjectPage)
