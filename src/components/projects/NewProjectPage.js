// @flow

import React, { Component } from 'react'
import { connect } from 'react-redux'
import navigateTo from '../../services/navigation'
import { CREATE_PROJECT } from '../../actionTypes'
import ProjectsForm from './ProjectsForm'

import type { Connector } from 'react-redux'
import type { Dispatch } from '../../types'
import type { ProjectPayload as Payload } from '../../types/projects'

type Props = {
  dispatch: Dispatch,
  createProject(payload: Payload): void
}

class NewProjectPage extends Component<Props> {
  handleSubmit = (payload: Payload) => {
    this.props.createProject(payload)
    navigateTo('/admin/projects')
  }

  render() {
    return (
      <div>
        <h2>Create new project</h2>
        <ProjectsForm onSubmit={this.handleSubmit} />
      </div>
    )
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    createProject: (payload: Payload) => dispatch({ type: CREATE_PROJECT, payload })
  }
}

const connector: Connector<{}, Props> = connect(null, mapDispatchToProps)
export default connector(NewProjectPage)
