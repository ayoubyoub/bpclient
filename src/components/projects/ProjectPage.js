// @flow

import React from 'react'
import { connect } from 'react-redux'
import { selectCurrentProject } from '../../selectors/projects'

import type { Connector } from 'react-redux'
import type { State, Dispatch } from '../../types'
import type { Project } from '../../types/projects'

type Props = {
  project: Project
}

type OwnProps = {
  match: {
    params: {
      id: number
    }
  }
}

function ProjectPage({ project }: Props) {
  return (
    <div>
      {project && (
        <div>
          <h2>{project.projectName}</h2>
          <div>{project.projectIdentifier}</div>
          <div>{project.description}</div>
        </div>
      )}
    </div>
  )
}

function mapStateToProps(state: State, ownProps: OwnProps) {
  const project = selectCurrentProject(state, Number(ownProps.match.params.id))
  return {
    project
  }
}

// eslint-disable-next-line no-unused-vars
function mapDispatchToProps(dispatch: Dispatch) {
  return {}
}

const connector: Connector<OwnProps, Props> = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default connector(ProjectPage)
