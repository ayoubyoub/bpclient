// @flow

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  FETCH_PROJECTS_IF_NEEDED,
  FETCH_PROJECTS,
  DELETE_PROJECT
} from '../../actionTypes'
import { selectProjects } from '../../selectors/projects'
import navigateTo from '../../services/navigation'
import ProjectsHeading from './ProjectsHeading'
import ProjectsList from './ProjectsList'

import type { Dispatch, State } from '../../types'
import type { ProjectsState } from '../../types/projects'
import type { Connector } from 'react-redux'

type Props = {
  projects: ProjectsState,
  match: {
    url: string
  },
  fetchProjectsIfNeeded(): void,
  deleteProject(id: number): void,
  fetchProjects(): void
}

class ProjectsPage extends Component<Props> {
  componentDidMount() {
    this.props.fetchProjectsIfNeeded()
  }

  handleDeleteProject = (id: number) => {
    if (window.confirm('Do you really want to delete this project?')) {
      this.props.deleteProject(id)
    }
  }

  handleNewProject = () => {
    const { url } = this.props.match
    navigateTo(`${url}/new`)
  }

  handleEditProject = (id: number) => {
    const { url } = this.props.match
    navigateTo(`${url}/${id}/edit`)
  }

  handleReloadProjects = () => {
    this.props.fetchProjects()
  }

  render() {
    const { items: projects, loading } = this.props.projects
    const { url } = this.props.match

    return (
      <div>
        <ProjectsHeading
          loading={loading}
          projects={projects}
          onNewProject={this.handleNewProject}
          onReloadProjects={this.handleReloadProjects}
        />

        <ProjectsList
          loading={loading}
          projects={projects}
          url={url}
          onEditProject={this.handleEditProject}
          onDeleteProject={this.handleDeleteProject}
        />
      </div>
    )
  }
}

function mapStateToProps(state: State) {
  return {
    projects: selectProjects(state)
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchProjectsIfNeeded: () => dispatch({ type: FETCH_PROJECTS_IF_NEEDED }),
    deleteProject: (id: number) => dispatch({ type: DELETE_PROJECT, id }),
    fetchProjects: () => dispatch({ type: FETCH_PROJECTS })
  }
}

const connector: Connector<{}, Props> = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default connector(ProjectsPage)
