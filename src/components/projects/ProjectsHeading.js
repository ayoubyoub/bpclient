// @flow

import React from 'react'

import type { Projects } from '../../types/projects'

type Props = {
  loading: boolean,
  projects: Projects,
  onNewProject: () => void,
  onReloadProjects: () => void
}

export default function ProjectsHeading({
  loading,
  projects,
  onNewProject,
  onReloadProjects
}: Props) {
  return (
    <div>
      <div className="projects-heading">
        <h2 className="projects-heading__title">Projects</h2>
        <button
          className="btn projects-heading__btn"
          onClick={onNewProject}
          disabled={loading}
        >
          New Project
        </button>
        <button
          className="btn projects-heading__btn"
          onClick={onReloadProjects}
          disabled={loading || projects.length === 0}
        >
          Reload Projects
        </button>
      </div>
    </div>
  )
}
