// @flow

import React from 'react'
import { Link } from 'react-router-dom'

import type { Projects } from '../../types/projects'

type Props = {
  loading: boolean,
  projects: Projects,
  url: string,
  onEditProject: (id: number) => void,
  onDeleteProject: (id: number) => void
}

export default function ProjectsList(props: Props) {
  const { loading, projects, url, onEditProject, onDeleteProject } = props

  if (loading) return <p>Loading...</p>
  if (projects.length === 0) return <div>No projects.</div>

  return (
    <ul className="projects">
      {projects.map(project => (
        <li className="projects__item" key={project.id}>
          <Link className="projects__title" to={`${url}/${project.id}`}>
            {project.projectName}
          </Link>
          <button
            className="btn projects__btn"
            onClick={() => onEditProject(project.id)}
            title="Edit"
          >
            <i className="fa fa-pencil-square-o" />
          </button>
          <button
            className="btn projects__btn"
            onClick={() => onDeleteProject(project.id)}
            title="Delete"
          >
            <i className="fa fa-trash-o" />
          </button>
        </li>
      ))}
    </ul>
  )
}
