// @flow

import React from 'react'
import { Route, Switch, Redirect, Link } from 'react-router-dom'
import ProjectsPage from './projects/ProjectsPage'
import NewProjectPage from './projects/NewProjectPage'
import EditProjectPage from './projects/EditProjectPage'
import ProjectPage from './projects/ProjectPage'

type Props = {
  match: {
    url: string
  }
}

export default function AdminPage({ match: { url } }: Props) {
  return (
    <div>
      <div className="header">
        <div className="container">
          <Link to="/admin" className="header__brand">
            Admin
          </Link>
        </div>
      </div>

      <div className="container">
        <Switch>
          <Route
            exact
            path={`${url}`}
            render={() => <Redirect to={`${url}/projects`} />}
          />
          <Route exact path={`${url}/projects`} component={ProjectsPage} />
          <Route exact path={`${url}/projects/new`} component={NewProjectPage} />
          <Route exact path={`${url}/projects/:id`} component={ProjectPage} />
          <Route
            exact
            path={`${url}/projects/:id/edit`}
            component={EditProjectPage}
          />
          <Redirect to="/error" />
        </Switch>
      </div>
    </div>
  )
}
