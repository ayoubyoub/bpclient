import { takeLatest, put, call } from 'redux-saga/effects'
import { createOrUpdateProjectInAPI } from '../../services/projects'
import navigateTo from '../../services/navigation'

function* updateProject(action) {
  yield put({ type: 'UPDATE_PROJECT_PENDING' })

  try {
    const updatedProject = yield call(createOrUpdateProjectInAPI, action.payload)
    yield put({ type: 'UPDATE_PROJECT_SUCCESS', payload: updatedProject })
    navigateTo('/admin/projects')
  } catch (error) {
    yield put({ type: 'UPDATE_PROJECT_FAILURE' })
    console.error(error) // eslint-disable-line
    yield put(navigateTo('/error'))
  }
}

export default function* watchUpdateProject() {
  yield takeLatest('UPDATE_PROJECT', updateProject)
}
