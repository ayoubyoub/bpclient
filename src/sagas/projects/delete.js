import { takeLatest, put, call } from 'redux-saga/effects'
import { deleteProjectFromApi } from '../../services/projects'
import navigateTo from '../../services/navigation'

function* deleteProject(action) {
  yield put({ type: 'DELETE_PROJECT_PENDING', id: action.id })

  try {
    yield call(deleteProjectFromApi, action.id)
    yield put({ type: 'DELETE_PROJECT_SUCCESS', id: action.id })
  } catch (error) {
    yield put({ type: 'DELETE_PROJECT_FAILURE' })
    console.error(error) // eslint-disable-line
    yield put(navigateTo('/error'))
  }
}

export default function* watchDeleteProject() {
  yield takeLatest('DELETE_PROJECT', deleteProject)
}
