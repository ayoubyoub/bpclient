import { takeLatest, put, call, select } from 'redux-saga/effects'
import { fetchProjectsFromApi } from '../../services/projects'
import navigateTo from '../../services/navigation'
import { selectProjects } from '../../selectors/projects'

function* fetchProjects() {
  yield put({ type: 'FETCH_PROJECTS_PENDING' })

  try {
    const projectsFromApi = yield call(fetchProjectsFromApi)
    yield put({ type: 'FETCH_PROJECTS_SUCCESS', payload: projectsFromApi })
  } catch (error) {
    yield put({ type: 'FETCH_PROJECTS_FAILURE' })
    console.error(error) // eslint-disable-line
    yield put(navigateTo('/error'))
  }
}

export function* watchFetchProjects() {
  yield takeLatest('FETCH_PROJECTS', fetchProjects)
}

function* fetchProjectsIfNeeded() {
  const { items: projects } = yield select(selectProjects)
  if (projects.length === 0) {
    yield call(fetchProjects)
  }
}

export function* watchFetchProjectsIfNeeded() {
  yield takeLatest('FETCH_PROJECTS_IF_NEEDED', fetchProjectsIfNeeded)
}
