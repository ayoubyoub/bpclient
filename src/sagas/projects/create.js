import { takeLatest, put, call } from 'redux-saga/effects'
import { createOrUpdateProjectInAPI } from '../../services/projects'
import navigateTo from '../../services/navigation'

function* createProject(action) {
  yield put({ type: 'CREATE_PROJECT_PENDING' })
  try {
    const newProject = yield call(createOrUpdateProjectInAPI, action.payload)
    yield put({ type: 'CREATE_PROJECT_SUCCESS', payload: newProject })
    navigateTo('/admin/projects')
  } catch (error) {
    yield put({ type: 'CREATE_PROJECT_FAILURE' })
    console.error(error) // eslint-disable-line
    yield put(navigateTo('/error'))
  }
}

export default function* watchCreateProject() {
  yield takeLatest('CREATE_PROJECT', createProject)
}
