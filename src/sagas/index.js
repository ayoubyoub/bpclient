import { all } from 'redux-saga/effects'
import { watchFetchProjectsIfNeeded, watchFetchProjects } from './projects/fetch'
import watchDeleteProject from './projects/delete'
import watchCreateProject from './projects/create'
import watchUpdateProject from './projects/update'

export default function* rootSaga() {
  yield all([
    watchFetchProjectsIfNeeded(),
    watchFetchProjects(),
    watchDeleteProject(),
    watchCreateProject(),
    watchUpdateProject()
  ])
}
