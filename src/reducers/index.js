// @flow

import { combineReducers } from 'redux'
import projects from './projects'

const entitiesReducer = combineReducers({
  projects
})

const rootReducer = combineReducers({
  entities: entitiesReducer
})

export default rootReducer
