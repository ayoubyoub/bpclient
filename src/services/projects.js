// @flow

import type { Project, Projects } from '../types/projects'

import service from './Api'

export function fetchProjectsFromApi(): Promise<Project> | Promise<Projects> {
  return service.get('/projects')
}

export function createOrUpdateProjectInAPI(payload: Project): Promise<Project> {
  return service.post('/projects', payload)
}

export function deleteProjectFromApi(id: number): Promise<number> {
  return service.delete(`/projects/${id}`)
}
