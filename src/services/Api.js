// @flow

import axios from 'axios'

import type { Axios } from 'axios'

const API_ROOT = 'http://localhost:8080/api'

class ApiService {
  client: Axios

  constructor() {
    const client = axios.create({
      baseURL: API_ROOT,
      timeout: 10000,
      headers: {
        'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJmdWxsTmFtZSI6IkF5b3ViIiwiaWQiOiIxIiwiZXhwIjoxNTYyNDk0MzgyLCJpYXQiOjE1NjIxOTQzODIsInVzZXJuYW1lIjoic2lzLmF5b3ViLnlvdWJAZ21haWwuY29tIn0.4yIPiKiJKWHYw5huVS1-96yZF5PEW-v6oMRWd6yozYH9Nt2wRBNA9NLWIDSojBd0JE3BtFc70CJ5mX6V_SYcjw',
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    })

    this.client = client
  }

  get(path: string): Promise<Object> | Promise<Array<Object>> {
    return this.client.get(path).then(response => response.data)
  }

  post(path: string, payload: Object): Promise<Object> {
    return this.client.post(path, payload).then(response => response.data)
  }
  
  delete(path: string): Promise<number> {
    return this.client.delete(path).then(response => response.data)
  }
}

export default new ApiService()
