// @flow

import type { Store as ReduxStore, Dispatch as ReduxDispatch } from 'redux'
import type { ProjectsState, ProjectsAction } from './projects'

export type ReduxInitAction = { type: '@@INIT' }
export type Action = ReduxInitAction | ProjectsAction

export type State = {
  entities: {
    projects: ProjectsState
  }
}

export type Store = ReduxStore<State, Action>
export type Dispatch = ReduxDispatch<Action>
