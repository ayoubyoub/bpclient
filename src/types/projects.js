// @flow

export type Project = {
  +id: number,
  +projectName: string,
  +projectIdentifier: string,
  +description: string
}

export type ProjectPayload = $Diff<Project, { id: number }>

export type Projects = Array<Project>

export type ProjectsState = {
  +items: Projects,
  +loading: boolean
}

export type ProjectsAction =
  | { type: 'FETCH_PROJECTS' }
  | { type: 'FETCH_PROJECTS_IF_NEEDED' }
  | { type: 'FETCH_PROJECTS_PENDING' }
  | { type: 'FETCH_PROJECTS_SUCCESS', payload: Projects }
  | { type: 'FETCH_PROJECTS_FAILURE' }
  | { type: 'DELETE_PROJECT' }
  | { type: 'DELETE_PROJECT_PENDING', id: number }
  | { type: 'DELETE_PROJECT_SUCCESS', id: number }
  | { type: 'DELETE_PROJECT_FAILURE' }
  | { type: 'CREATE_PROJECT', payload: Project }
  | { type: 'CREATE_PROJECT_PENDING' }
  | { type: 'CREATE_PROJECT_SUCCESS', payload: Project }
  | { type: 'CREATE_PROJECT_FAILURE' }
  | { type: 'UPDATE_PROJECT', payload: Project }
  | { type: 'UPDATE_PROJECT_PENDING' }
  | { type: 'UPDATE_PROJECT_SUCCESS', payload: Project }
  | { type: 'UPDATE_PROJECT_FAILURE' }
